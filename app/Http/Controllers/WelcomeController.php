<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class WelcomeController extends Controller
{
    public static $counter=-1;
    public $greetingMessage="Hello friend, Do you like to read something interesting?";
    public $defaultMessage="Sorry, my database is poor in words, so I can't identify your message. Can I get you something to read?";
    public $goodByeMessage="OK cool, if you need me don't hesitate to say: *hi*";
    public function index(Request $request)
    {
        $verify_token = "fb_time_bot";
        $hub_verify_token = null;
        if (isset($_REQUEST['hub_challenge'])) {
            $challenge = $_REQUEST['hub_challenge'];
            $hub_verify_token = $_REQUEST['hub_verify_token'];
        }
        if ($hub_verify_token === $verify_token) {
            echo $challenge;
        }
    }

    public function getMessages(Request $request)
    {

      $input = json_decode(file_get_contents('php://input'), true);
      $sender = $input['entry'][0]['messaging'][0]['sender']['id'];
      $postback = isset($input['entry'][0]['messaging'][0]['postback']['payload']) ? $input['entry'][0]['messaging'][0]['postback']['payload']: '' ;
      if($postback) { 
        
          
          // If Page receives Postback, process the Postback and prepare content to reply
            
          switch($postback) {
          
            case 'get_started':
              $reply = $this->greetingMessage;
              break;
          
          }
        
         $responseJSON = '{
          "recipient":{
            "id":"'.$sender.'"
          },
          "message": {
                  "text":"'. $reply .'"
              }
        }';

        $this->Reply($responseJSON,$input);
        
      }else{
        $this->ReplyToTheUser($input);
      }

    }
    
    /**
    *
    * Handel user's messages.
    */
    public function ReplyToTheUser($input)
    {
      $approve = array("hi","yes","YES","Yes","HI","Hi","more","More","MORE","Sure","SURE","OK","sure","ok","alrigt","Alrigt","of course","Of course");
      $refuse = array("no","No","NO","Nope","nope");
      $greeting = array("Get Started","hi","HI","Hi","Hey","HEY","hey");

        $message = $input['entry'][0]['messaging'][0]['message']['text'];

        switch (True) {
            case in_array($message,$greeting):
                $this->Reply($this->SetMessage($this->greetingMessage,$input), $input);
                break;

            case in_array($message,$approve):
                $this->Reply($this->SetReplyMessage($this->GetTopic(),$input), $input);
                break;

                case in_array($message,$refuse):
                $this->Reply($this->SetMessage($this->goodByeMessage,$input), $input);
                break;
                
            default:
                $this->Reply($this->SetMessage($this->defaultMessage,$input), $input);
                self::$counter=-1;
        }
    }

    public function SetMessage($message,$input){
      $sender = $input['entry'][0]['messaging'][0]['sender']['id'];
      
      $jsonData = '{
        "recipient":{
            "id":"' . $sender . '"
        }, 
        "message":{

            "text": "'.$message.'",
           
        }
    }';

    return $jsonData;
    }


    public function GetTopic()
    {
        $topics = array(
        'https://medium.freecodecamp.org/how-i-went-from-fashion-model-to-software-engineer-in-1-year-a7399a40d9e7',
        'https://medium.freecodecamp.org/become-how-i-went-from-selling-food-in-the-street-to-working-for-top-firms-in-tech-6aa61a2d0629',
        'https://medium.com/the-mission/how-writing-1000-words-a-day-changed-my-life-cf72453b8fef'
        );
          
        return $topics[array_rand($topics,1)];
     
    }

    public function SetReplyMessage($url, $input)
    {
        $sender = $input['entry'][0]['messaging'][0]['sender']['id'];
      
        if ($url) {
            $jsonData = '{
              "recipient":{
                  "id":"' . $sender . '"
              }, 
              "message":{

                  "text": "'.$url.'\n\nIf you are not satisfied yet just type: *more*",
                  
              }
          }';
        }else{
          $jsonData = '{
            "recipient":{
                "id":"' . $sender . '"
            }, 
            "message":{

                "text": "So sorry! there is nothing more to read. '. self::$counter.'",
               
            }
        }';

        }

        return $jsonData;
    }
    public function Reply($jsonData, $input)
    {

      //API Url
        $url = 'https://graph.facebook.com/v2.6/me/messages?access_token=' . env("PAGE_ACCESS_TOKEN");

      //Initiate cURL.
        $ch = curl_init($url);

      //Encode the array into JSON.
        $jsonDataEncoded = $jsonData;

      //Tell cURL that we want to send a POST request.
        curl_setopt($ch, CURLOPT_POST, 1);

      //Attach our encoded JSON string to the POST fields.
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);

      //Set the content type to application/json
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
      //curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
        $result = curl_exec($ch);
        
    }
}
